car_list = []

def buy_car(car_index):
    car = car_list[car_index]
    car_list.pop(car_index)
    print(f"{car['brand']} {car['model']} comprado com sucesso!")

def register_car():
    car_brand = input("Digite a marca do carro: ")
    car_model = input("Digite o modelo do carro: ")
    car_price = float(input("Digite o preço do carro: "))

    car_list.append({
        "brand": car_brand,
        "model": car_model,
        "price": car_price
    })
    print(f"Carro {car_brand} {car_model} cadastrado com sucesso!")

def list_cars():
    if len(car_list) == 0:
        print("Nenhum carro cadastrado.")
        return

    print("\nLISTAGEM DE CARROS\n")

    for car in car_list:
        print("Número 1:")
        print(f"  Marca: {car['brand']}")
        print(f"  Modelo: {car['model']}")
        print(f"  Preço: {car['price']}")
        print("------------")


print("\nBem-vindo(a) à concessionária Python Car! :)\n")
print("Para iniciar, escolha uma das opções abaixo:")

while True:
    print("\n1 - Comprar")
    print("2 - Cadastrar")
    print("3 - Listar")
    print("4 - Sair")

    option = int(input("\n"))

    if option == 1:
      list_cars()
      car_index = int(input("Digite o número do carro que deseja comprar: "))
      buy_car(car_index - 1)
    elif option == 2:
      register_car()
    elif option == 3:
      list_cars()
    else: 
        break
    
    print("\nO que deseja fazer agora?")