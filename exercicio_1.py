# ---- EXERCICIO 1 ----
numero_1 = int(input("Digite um número: "))
numero_2 = int(input("Digite outro número: "))

if numero_1 > numero_2:
    print("Maior número:", numero_1)
else:
    print("Maior número:", numero_2)

# ---- EXERCICIO 2 ----
percentual_crescimento = float(input("\nDigite o percentual de crescimento: "))

if percentual_crescimento > 0:
    print("Houve crescimento de produção na empresa!")
elif percentual_crescimento < 0:
    print("A produção da empresa diminuiu :(")
else:
    print("Não houve crescimento de produção na empresa.")