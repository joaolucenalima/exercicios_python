# ---- EXERCICIO 2.1 ----
numero_1 = int(input("Digite um número: "))
numero_2 = int(input("Digite outro número: "))

for number in range(numero_1, numero_2):
    print(number)

# ---- EXERCICIO 2.2 ----
numero_tabuada = int(input("\nDigite um número para ver a tabuada: "))
for number in range(1, 11):
    print(f"{numero_tabuada} x {number} = {numero_tabuada * number}")